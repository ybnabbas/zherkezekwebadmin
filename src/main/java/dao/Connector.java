package dao;

import utils.Prop;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    static Connection connection;

    public static Connection getConnection() {
        try {
            if (connection == null) {
                Class.forName(Prop.getString("db_driver"));
                connection = DriverManager.getConnection(Prop.getString("db_url"), Prop.getString("db_user"), Prop.getString("db_password"));
            }
            return connection;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }


    }
}
