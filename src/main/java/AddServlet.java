import moderator.Moderator;
import org.json.JSONObject;
import utils.Prop;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;


@WebServlet("/add")
public class AddServlet extends HttpServlet {
    HttpServletRequest req;
    HttpServletResponse resp;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
       this.req=req;
       this.resp=resp;
        Moderator moderator=(Moderator)req.getSession().getAttribute("zherKezekAuth");
        String fullName = req.getParameter("fullname");
        String iin = req.getParameter("iin");
        String applyDate = req.getParameter("apply_date").replace("-",".");
        JSONObject plot= new JSONObject();
        plot.put("fullname",fullName);
        plot.put("iin",iin);
        plot.put("apply_date",applyDate);
        plot.put("id_village",moderator.getVillageId());
        plot.put("confirm",false);

        Client client= ClientBuilder.newClient();
        Response response=client.target(Prop.getString("server")+"adduser").request().post(Entity.entity(plot.toString(), MediaType.APPLICATION_JSON_TYPE));
       if (response.getStatus()==200){

           redirectResult(true);
       }else {
          redirectResult(false);
       }

    }

    private void redirectResult(boolean success) throws ServletException, IOException {
        String redirectAdr = "/add.jsp";
        if (success){
            req.setAttribute("success", success);
            req.setAttribute("user",req.getParameter("fullname") + " базаға қосылды" );
        } else {
            req.setAttribute("success", false);

        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(redirectAdr);
        dispatcher.forward(req, resp);
    }
}
