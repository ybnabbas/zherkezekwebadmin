import org.json.JSONArray;
import utils.Prop;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/list")
public class ListServlet extends HttpServlet {

    HttpServletRequest req;
    HttpServletResponse resp;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.req = req;
        this.resp = resp;
//doit();

        redirect();
    }

    private void doit(){
        Client client = ClientBuilder.newClient();
        Response response=client.target("http://78.40.108.89:8080/ZherKezekServer/sairam/plot/1/2").request().get();

        String e= response.readEntity(String.class);
        JSONArray jsonArray = new JSONArray(e);
        System.out.println(jsonArray.toString());
        req.setAttribute("list",jsonArray);
    }

    private void redirect() throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/list.jsp");
        dispatcher.forward(req, resp);
    }
}
