package utils;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Prop {
    static Properties properties = new Properties();
    static InputStream inputStream;

    public static String getString(String value) {

        try {
            inputStream = Prop.class.getClassLoader().getResourceAsStream("db.properties");
            properties.load(inputStream);
            return properties.getProperty(value);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
