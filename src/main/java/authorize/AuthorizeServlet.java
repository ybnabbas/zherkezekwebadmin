package authorize;

import moderator.Moderator;
import moderator.ModeratorService;
import utils.Prop;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/authorize")
public class AuthorizeServlet extends HttpServlet {
    ModeratorService mService = new ModeratorService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login=req.getParameter("login");
        String password=req.getParameter("password");
        Moderator moderator = mService.getModerator(login,password);
        String redirectAdr = null;
if (moderator==null){
redirectAdr="/index.jsp";
String wrongMessage="Логин немесе құпия сөз дұрыс емес!";
req.setAttribute("message",wrongMessage);
}
else {
    HttpSession authSession=req.getSession();

    System.out.println(authSession.getMaxInactiveInterval());
    authSession.setAttribute("zherKezekAuth",moderator);
   // authSession.setAttribute("village",);
    redirectAdr="/main";

}
        RequestDispatcher dispatcher=getServletContext().getRequestDispatcher(redirectAdr);
        dispatcher.forward(req,resp);
    }
}
